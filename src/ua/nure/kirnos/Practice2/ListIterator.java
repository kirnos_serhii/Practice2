
package ua.nure.kirnos.practice2;

import java.util.Iterator;

/**
 * An iterator for lists iterator that allows browse list.
 *
 * @author *
 * @version 1.0
 */
interface ListIterator extends Iterator<Object> {

	/**
	 * Method returns true if this list iterator has more elements when traversing
	 * the list in the reverse direction.
	 *
	 * @return {@code true} if the list iterator has more elements when
     *         traversing the list in the reverse direction
	 */
	boolean hasPrevious();

	/**
	 * Returns the previous element in the list and moves the cursor
	 * position backwards.
	 *
	 * @return the previous element in the list
	 */
	Object previous();

	/**
	 * Replaces the last element returned by {@link #next} or {@link #previous}.
	 *
	 * @param e the element with which to replace the last element returned by
     *          {@code next} or {@code previous}
	 */
	void set(Object e);

	/**
	 * Remove the last element returned by {@link #next} or {@link #previous}.
	 */
	void remove();
}

package ua.nure.kirnos.practice2;

/**
 * Interface for create ListsIterator object.
 *
 * @author Serhii
 * @version 1.0
 */
public interface ListIterable {

	/**
	 * Create ListsIterator.
	 *
	 * @return object ListIterator
	 */
	ListIterator listIterator();
}

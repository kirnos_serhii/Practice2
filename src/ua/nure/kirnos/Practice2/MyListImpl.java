package ua.nure.kirnos.practice2;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Doubly-linked list implementation.
 *
 * @see MyList
 * @author Serhii
 *
 */
public class MyListImpl implements MyList, ListIterable {
	/**
	 * Pointer to first element.
	 */
	private Element head;
	/**
	 * Pointer to last element.
	 */
	private Element tail;
	/**
	 * Number of items in List
	 */
	private int size;

	/**
	 * Structure for store one list item.
	 *
	 * @author Serhii
	 *
	 */
	private class Element {
		/**
		 * Pointer to the next item
		 */
		Element next;
		/**
		 * Pointer to the previous item
		 */
		Element prev;
		/**
		 * Storage information
		 */
		Object data;

		/**
		 * Constructor for the create new element and set parameters.
		 *
		 * @param d storage information
		 * @param n pointer to the next item
		 * @param p pointer to the previous item
		 */
		public Element(Object d, Element n, Element p) {
			data = d;
			next = n;
			prev = p;
		}
	}

	/**
	 * Class that implements interface Iterator<Object>.
	 *
	 * @author Serhii
	 *
	 */
	private class IteratorImpl implements Iterator<Object> {
		protected Element nextReturn = head;
		protected boolean p = true;

		/* (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			if (nextReturn.next != tail) {
				return true;
			}
			return false;
		}

		/* (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		@Override
		public Object next() {
			if (nextReturn.next == tail) {
				throw new NoSuchElementException();
			}
			nextReturn = nextReturn.next;
			p = false;
			return nextReturn.data;
		}

		/* (non-Javadoc)
		 * @see java.util.Iterator#remove()
		 */
		@Override
		public void remove() {
			if ((nextReturn == head) || p) {
				throw new IllegalStateException();
			}
			nextReturn = nextReturn.prev;
			MyListImpl.this.remove(nextReturn.next);
			p = true;
		}
	}

	/**
	 * Class that implements interface ListIterator.
	 *
	 * @author Serhii
	 *
	 */
	private class ListIteratorImpl extends IteratorImpl implements ListIterator {
		protected Element previousReturn = head;
		protected Element set = null;

		/* (non-Javadoc)
		 * @see ua.nure.kirnos.practice2.MyListImpl.IteratorImpl#hasNext()
		 */
		@Override
		public boolean hasNext() {
			if (nextReturn.next != tail) {
				return true;
			}
			return false;
		}

		/* (non-Javadoc)
		 * @see ua.nure.kirnos.practice2.MyListImpl.IteratorImpl#next()
		 */
		@Override
		public Object next() {
			if (nextReturn.next == tail) {
				throw new NoSuchElementException();
			}
			nextReturn = nextReturn.next;
			if (previousReturn == head) {
				previousReturn = previousReturn.next;
			}
			previousReturn = previousReturn.next;
			set = previousReturn;
			return nextReturn.data;
		}

		/* (non-Javadoc)
		 * @see ua.nure.kirnos.practice2.MyListImpl.IteratorImpl#remove()
		 */
		@Override
		public void remove() {
			if (set == null) {
				throw new IllegalStateException();
			}
			if (nextReturn == set)
				nextReturn = nextReturn.prev;
			else {
				previousReturn = previousReturn.next;
			}
			MyListImpl.this.remove(set);
			set = null;
		}

		/* (non-Javadoc)
		 * @see ua.nure.kirnos.practice2.ListIterator#hasPrevious()
		 */
		@Override
		public boolean hasPrevious() {
			if (previousReturn == head) {
				return false;
			}
			if (previousReturn.prev != head) {
				return true;
			}
			return false;
		}

		/* (non-Javadoc)
		 * @see ua.nure.kirnos.practice2.ListIterator#previous()
		 */
		@Override
		public Object previous() {
			if (previousReturn.prev == head) {
				throw new NoSuchElementException();
			}
			previousReturn = previousReturn.prev;
			nextReturn = nextReturn.prev;
			set = nextReturn;
			return previousReturn.data;
		}

		/* (non-Javadoc)
		 * @see ua.nure.kirnos.practice2.ListIterator#set(java.lang.Object)
		 */
		@Override
		public void set(Object e) {
			if (set == null) {
				throw new IllegalStateException();
			}
			set.data = e;
			set = null;
		}
	}

	/**
	 * Constructor without parameters.
	 */
	public MyListImpl() {
		head = new Element(null, null, null);
		tail = new Element(null, null, null);
		head.next = tail;
		tail.prev = head;
		size = 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.ListIterable#listIterator()
	 */
	@Override
	public ListIterator listIterator() {
		return new ListIteratorImpl();
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.MyList#add(java.lang.Object)
	 */
	@Override
	public void add(Object e) {
		Element newEll = new Element(e, tail, tail.prev);
		tail.prev.next = newEll;
		tail.prev = newEll;
		if (head.next == tail) {
			head.next = newEll;
		}
		size++;
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.MyList#clear()
	 */
	@Override
	public void clear() {
		// ???
		Element el = tail.prev;
		while (el.prev != null) {
			Element prevel = el.prev;
			el.data = null;
			el.next = null;
			el.prev = null;
			el = prevel;
		}
		head.next = tail;
		tail.prev = head;
		size = 0;
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.MyList#remove(java.lang.Object)
	 */
	@Override
	public boolean remove(Object o) {
		if (o == null) {
			for (Element x = head.next; x != tail; x = x.next) {
				if (x.data == null) {
					remove(x);
					return true;
				}
			}
		} else {
			for (Element x = head.next; x != tail; x = x.next) {
				if (o.equals(x.data)) {
					remove(x);
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Deletes element.
	 *
	 * @param el pointer to the item to delete
	 */
	void remove(Element el) {
		el.data = null;
		if (el.next != tail) {
			el.next.prev = el.prev;
		} else {
			tail.prev = el.prev;
			// tail.next = null;
		}
		if (el != head) {
			el.prev.next = el.next;
		} else {
			head.next = el.next;
			// head.prev = null;
		}
		el.next = null;
		el.prev = null;
		size--;
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.MyList#toArray()
	 */
	@Override
	public Object[] toArray() {
		Object[] arr = new Object[size];
		Element el = head.next;
		int i = 0;
		while (el.next != null) {
			arr[i++] = el.data;
			el = el.next;
		}
		return arr;
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.MyList#size()
	 */
	@Override
	public int size() {
		return size;
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.MyList#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(Object o) {
		if (o == null) {
			for (Element x = head.next; x != tail; x = x.next) {
				if (x.data == null) {
					return true;
				}
			}
		} else {
			for (Element x = head.next; x != tail; x = x.next) {
				if (o.equals(x.data)) {
					return true;
				}
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see ua.nure.kirnos.practice2.MyList#containsAll(ua.nure.kirnos.practice2.MyList)
	 */
	@Override
	public boolean containsAll(MyList c) {
		for (Object tmp : c.toArray()) {
			if (this.contains(tmp) == false) {
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder outres = new StringBuilder();
		outres.append("[");
		Element el = head.next;
		while (el.next != null) {
			outres.append(el.data);
			el = el.next;
			if (el.next != null) {
				outres.append(", ");
			}
		}
		outres.append("]");
		return outres.toString();
	}
}

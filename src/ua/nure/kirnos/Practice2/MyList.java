package ua.nure.kirnos.practice2;

/**
 * An iterator for lists that allows change list.
 *
 * @author *
 * @version 1.0
 */
public interface MyList extends Iterable<Object> {

	/**
	 * Appends the specified element to the end of this list.
	 *
	 * @param e the element to insert
	 */
	void add(Object e);

	/**
	 * Removes all of the elements from this list.
	 */
	void clear();

	/**
	 * removes the first occurrence of the specified element
	 * from this list.
	 *
	 * @param o item to remove
	 * @return {@code true} if the item has been deleted
	 */
	boolean remove(Object o);

	/**
	 * Returns an array containing all of the elements
	 * in this list in proper sequence.
	 *
	 * @return array of elements
	 */
	Object[] toArray();

	/**
	 * Returns the number of elements in this list.
	 *
	 * @return number of elements in this list
	 */
	int size();

	/**
	 * Returns true if this list contains the specified element.
	 *
	 * @param o element to searsh
	 * @return {@code true} if the element was found
	 */
	boolean contains(Object o);

	/**
	 * Returns true if this list contains all of the elements
	 * of the specified list.
	 *
	 * @param c array of elements for searsh
	 * @return {@code true} if the all items were found
	 */
	boolean containsAll(MyList c);
}
